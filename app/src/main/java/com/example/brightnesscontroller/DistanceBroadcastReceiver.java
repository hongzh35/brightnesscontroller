package com.example.brightnesscontroller;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


public class DistanceBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = "MyBroadcastReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.hasExtra("data")) {
            int brightness = intent.getIntExtra("data", 100);
            Log.d(TAG, ""+brightness);
            Toast.makeText(context, "current brightness: "+brightness, Toast.LENGTH_LONG).show();

            // set the brightness
            Window window = ((Activity) context).getWindow();
            WindowManager.LayoutParams lp = window.getAttributes();
            lp.screenBrightness = brightness / 255.0f;
            window.setAttributes(lp);
        }

    }
}
