package com.example.brightnesscontroller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private int brightness;
    public BroadcastReceiver br;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        br = new DistanceBroadcastReceiver();
        IntentFilter filter = new IntentFilter("com.example.broadcast.MY_NOTIFICATION");
        this.registerReceiver(br, filter);
        brightness = getScreenBrightness(this);
        Toast.makeText(this, "current system brightness:" + brightness,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        unregisterReceiver(br);
    }

    private int getScreenBrightness(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        int defVal = 125;
        return Settings.System.getInt(contentResolver,
                Settings.System.SCREEN_BRIGHTNESS, defVal);
    }


    public void light(View view) {
        Intent intent = new Intent();
        intent.setAction("com.example.broadcast.MY_NOTIFICATION");
        brightness = 200;
        brightness = Math.min(brightness, 255);
        intent.putExtra("data", brightness);
        sendBroadcast(intent);
    }

    public void dark(View view) {
        Intent intent = new Intent();
        intent.setAction("com.example.broadcast.MY_NOTIFICATION");
        brightness = 50;
        intent.putExtra("data", brightness);
        sendBroadcast(intent);
    }
}